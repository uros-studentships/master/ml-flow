# ML Flow
This is just another one in a million repos containing configuration for ML Flow experiment tracking service.
MinIO S3 service is used to store models, while Postgres database handles experiments and other classical
data.

Most of the configurations in `docker-compose.yml` are hard-coded as this repo is only meant to support my
work on my master thesis so I tried purposefully to put as little effort as possible in order to focus on
models and task at hand.

## Usage
First, folders `buckets`, and `postgres_data` must be created. After that standard `docker compose up` should
do the trick.

